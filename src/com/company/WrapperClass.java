package com.company;

public class WrapperClass {
    private int i;

    WrapperClass(int i) {
        this.i = i;
    }

    @Override
    public String toString() {
        return Integer.toString(i);
    }
}
